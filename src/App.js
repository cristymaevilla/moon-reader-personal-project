import {Fragment} from 'react';
import {useState, useEffect} from'react';
import {Container} from 'react-bootstrap';
import Navbar from './components/navbar';  
import{BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch, useLocation, useRouteMatch} from 'react-router-dom';

// import { Route, Routes, useLocation } from "react-router";
import { motion, AnimatePresence } from "framer-motion";
import './App.css';
// pages:
import Home from './pages/home';
import Book from './pages/book';
import Author from './pages/author';
import About from './pages/about';
import Links from './pages/links';
import Search from './pages/book-search';


// NOTE: I removed the <Router> and transfered it to index.js
function App() {

  // TO WORK: use two types of navbar (put props in app.js and include it the navbar component)
  const location = useLocation(); 
  const match = useRouteMatch();
   useEffect(() => {
    const currentPath = location.pathname;
    if(currentPath === "/"){console.log("navbar set to dark")}
    const searchParams = new URLSearchParams(location.search);
    console.log(`path: ${currentPath}`); 
  }, [location]);
   // https://pretagteam.com/question/use-effect-for-every-time-that-locationpathname-changes

    
  return (
            <div className="p-0 m-0" style={{overflow: "hidden"}}>                 
                  <Navbar/>
                  <AnimatePresence exitBeforeEnter>
                  <Switch key={location.pathname} location={location}> 
                    <Route exact path ="/" component ={Home} />             
                     <Route exact path ="/to-read-book" component ={Book} />
                     <Route exact path ="/author" component ={Author} />
                     <Route exact path ="/about" component ={About} />
                     <Route exact path ="/links" component ={Links} />
                     <Route exact path ="/search-book" component ={Search} />
                  </Switch>
                </AnimatePresence>
            </div>



  );
}

export default App;
