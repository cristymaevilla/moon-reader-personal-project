const featureData={
	author:"Philip Pullman",
	firstName:"Philip",
	lastName:"Pullman",
	id: "OL26545A",
	bookId:"OL17827992W",
	bookImageId:"8743802",
	imageurl:"https://covers.openlibrary.org/b/id/8743802-L.jpg",

	bookTitle:"The Book of Dust",
	bookTitle1stL:"The",
	bookTitle2ndL:"Book of",
	bookTitle3rdL:"Dust",  
}
// Check the spacing of the book title in home.js (must be 3 rows)
export default featureData;