import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter } from "react-router-dom";


/*import the Bootstrap CSS*/
import 'bootstrap/dist/css/bootstrap.min.css';
//the <Router> removed from app.js is added here as <BrowserRouter> so that we can use the "location" in app.js
ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// const  name = "John Smith";
// const user ={
//   firstName: "Jane",
//   lastName : 'Doe'

// }
// function formatName(user){
//   return user.firstName+ " "+ user.lastName;
// }
// const element= <h1>Hello, {formatName(user)}</h1>

// ReactDOM.render(
//   element,
//   document.getElementById('root')
//   );