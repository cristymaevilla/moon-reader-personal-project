import { Button , Row , Col}from 'react-bootstrap';
import { NavLink} from 'react-router-dom'; 
import { useEffect, useState} from 'react';
import featureData from '../data/data';
import Image from 'react-bootstrap/Image';
import Modal from 'react-bootstrap/Modal';
import PropTypes from 'prop-types';
import {motion} from 'framer-motion';


export default function BookInfo(bookProp) {

  const handleClose = () => setModalShow(false);

  const [modalShow, setModalShow] =useState(false);
  const bgStyle = {
    // backgroundImage: "url('https://covers.openlibrary.org/b/id/${Image}-M.jpg')",
    backgroundRepeat: "no repeat",
    backgroundSize: "cover",
    backgroundColor: "white"
      };
  return (
    <>
      <Button variant="primary" onClick={() => setModalShow(true)}>
        Launch static backdrop modal
      </Button>
    <Modal
      {...bookProp}
       size="lg"
      dialogClassName="container-fluid"
      aria-labelledby="example-custom-modal-styling-title"
      centered
      
    > <div className="m-0 p-0" style={{bgStyle}} >
        <div className="m-0 p-0 gradient-bg d-md-flex">
          <Modal.Header closeButton className="border-0 pb-1 d-block d-md-none d-flex justify-content-around">
            <h3  id="example-custom-modal-styling-title">
                sdgsdgsdgsd title
            </h3>
          </Modal.Header>
          <Modal.Header closeButton className="border-0  d-none d-md-flex position-absolute col-12  justify-content-end top">
          </Modal.Header>
          <div className="p-0 m-0 d-md-flex">
              <div className="p-2 d-flex justify-content-center ml-0 m-0 m-xl-4 col-12 col-md-4">
                  <div  className="p-2 mt-0 justify-content-center ml-0 m-0 m-xl-4 container-fluid position-absolute d-md-flex d-none">
                    <Image fluid src={`https://covers.openlibrary.org/b/id/${Image}-M.jpg`} className="book-card-image align-middle rounded shadow-1"/>
                  </div>
                  <Image fluid src={`https://covers.openlibrary.org/b/id/${Image}-M.jpg`} className="d-md-none book-card-image align-middle rounded shadow-1"/>
              </div>
              <Modal.Body className="pt-0 pl-5">
                <h3 id="example-custom-modal-styling-title"
                  className="border-0  d-none d-md-block mt-5" >
                    sdgsdgsdgdsgsd title
                </h3>
                <h5>by {featureData.author}</h5> 
                <p>1990 Edition will not be included here</p>
                <p>
                  gsdfgsdgsdgsdgsd descriptiondsafasdfafdafafafasfsafsa
                </p>
            </Modal.Body>
          </div>
        </div>
      </div>
    </Modal></>
  );
}
BookInfo.propTypes = {
    book:PropTypes.shape({

        key:PropTypes.string.isRequired,
        title:PropTypes.string.isRequired,
        covers:PropTypes.string.isRequired,
        description: PropTypes.string.isRequired
    })
}


