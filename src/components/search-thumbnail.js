import { Button , Row , Col}from 'react-bootstrap';
import { NavLink} from 'react-router-dom'; 
import { useEffect, useState} from 'react';
import featureData from '../data/data';
import Image from 'react-bootstrap/Image';
import Modal from 'react-bootstrap/Modal';
import PropTypes from 'prop-types';
import {useAnimation, motion} from 'framer-motion';
import loaderThumbnail from '../images/thumbnail-loader-sm.gif'; 
import {useInView} from 'react-intersection-observer';

export default function ResultThumbnail({thumbnailProp}) {

    const{key, title, cover_i, author_name }=thumbnailProp;

    // for Modal:
    const [modalShow, setModalShow] =useState(false);
    const [bookDescription, setBookDescription] =useState("No description available for this book.");
    function book(){
    	console.log(key);
    	fetch(`https://openlibrary.org${key}.json`)
    			.then (res=> res.json())
    			.then(data =>{
    				
    				if(data.description !== undefined){
    					if(data.description.value !== undefined){
    					    setBookDescription(data.description.value);setModalShow(true); 
    					    console.log(`VALUE: ${data.description.value}`) 
    					     }
    					else{
    						console.log(`description: ${data.description}`); setBookDescription(data.description);
    						setModalShow(true); 
    					}
    				}
    				
    				else{ setModalShow(true); console.log(data);}
    				
    					})
    }


    // for animate when scrolled or inview;
    const{ref, inView}=useInView();
    const animation= useAnimation();
useEffect(()=>{
  // console.log("use effect hook, inView=", inView);
  if(inView){
    animation.start({
      opacity:1,
      y:0,
      transition: {
            type: "spring",
            bounce: 0.4,
            transition:{duration: 1, delay: 0.5}
          }
    })
  }
  if(!inView){
    animation.start({
      opacity:0,
      y:-30
    })
        }
})

const handleClose = () => setModalShow(false);
    // const handleShow = () => setModalShow(true); 
function handleShow (){setModalShow(true); 
        // if(description.value !== undefined){
        //     setNewDescription(description.value); 
        //     console.log(`VALUE: ${description.value}`) 
        //      }
        // else{console.log(`description: ${description}`); setNewDescription(description); }
};

const thumbnail={
  initial:{
    opacity:0,
    y:-10
  },
  animate:{
    opacity:1,
    y:0,
    transition: {
          type: "spring",
          bounce: 0.4,
          duration: 0.8
        }
  }
}


  return (

  		<div 
      ref={ref}
       className="col-4 col-md-2 p-3 ">
  			<motion.Button 
        animate={animation}
        variant="light" className="thumbnail-img p-0 container-fluid m-0 border-0 hover-shine shadow-1" onClick={book}>
  			     <Image fluid src={`https://covers.openlibrary.org/b/id/${cover_i}.jpg`} className="rounded shadow-1"/>
  			</motion.Button>
            <Modal
                  {...thumbnailProp}
                  show={modalShow}
                  onHide={() => setModalShow(false)}
                  size="lg"
                  dialogClassName="container-fluid"
                  aria-labelledby="example-custom-modal-styling-title"
                  centered
            > 
                <div className="m-0 p-0" style={{
                    backgroundImage: `url('https://covers.openlibrary.org/b/id/${cover_i}-M.jpg')`,
                    backgroundRepeat: "no repeat",
                    backgroundSize: "cover",
                    backgroundColor: "white"
                }} >
                    <div className="m-0 p-0 gradient-bg d-md-flex">
                      <Modal.Header closeButton className="border-0 pb-1 d-block d-md-none d-flex justify-content-around">
                        <h3  id="example-custom-modal-styling-title">
                            {title}
                        </h3>
                      </Modal.Header>
                      <Modal.Header closeButton className="border-0  d-none d-md-flex position-absolute col-12  justify-content-end top">
                      </Modal.Header>
                      <div className="p-0 m-0 d-md-flex justify-content-around">
                          <div className="p-2 d-flex justify-content-center ml-0 m-0 m-xl-4 col-12 col-md-4">
                              <div  className="p-2  justify-content-center m-auto container-fluid d-md-flex d-none">
                                <Image fluid src={`https://covers.openlibrary.org/b/id/${cover_i}-M.jpg`} className="book-card-image align-top rounded shadow-1"/>
                              </div>
                              <Image fluid src={`https://covers.openlibrary.org/b/id/${cover_i}-M.jpg`} className="d-md-none book-card-image align-middle rounded shadow-1"/>
                          </div>
                          <Modal.Body className="pt-0 pl-5 col-md-10 col-12">
                            <h3 id="example-custom-modal-styling-title"
                              className="border-0  d-none d-md-block mt-5" >
                                {title}
                            </h3>
                            <h5>by: {author_name}</h5> 
                            <p>{bookDescription}</p>
                        </Modal.Body>
                      </div>
                    </div>
                  </div>
            </Modal>
  		</div>

  );
}
ResultThumbnail.propTypes = {
    result:PropTypes.shape({

        key:PropTypes.string.isRequired,
        title:PropTypes.string.isRequired,
        cover:PropTypes.string.isRequired,
        author: PropTypes.string.isRequired
    })
}



