import { Button , Row , Col, Container, Offcanvas, Form} from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav';
import { NavLink,  Link, useLocation, useParams} from 'react-router-dom'; 
import { useEffect, useState} from 'react';
import {motion} from 'framer-motion';
// IMAGES:
import mB from '../images/m-b.svg'; 
import oB from '../images/o-b.svg';
import nB from '../images/n-b.svg';
import moonB from '../images/moon-b.svg';

import mW from '../images/m-w.svg';
import oW from '../images/o-w.svg';
import nW from '../images/n-w.svg';
import moonW from '../images/moon-w.svg';

import logo from '../images/logo.svg'; 
import moon from '../images/moon.svg'; 
import moonRotating from '../images/moon-rotating.svg';

export default function Author(){
// search for book title:
const[keyword,setKeyword]=useState('');
const[query,setQuery]=useState('');
const[dark,setDark]=useState(true);
const location = useLocation();
const {params} = useParams();
const[isHovered,setIsHovered]=useState(false);
useEffect(()=>{
	console.log(location);
	 // showLocation()
		},[])
	
	
function show(){
	searchBook();
}
function showLocation(){
	console.log(location);console.log(params);
	console.log(`isdark? ${dark}`);
	if(location.pathname === "/"){setDark(true); console.log("dark")}
	else{setDark(false); console.log("light")}
}
function searchBook(){
	const q = keyword.replaceAll(" ", '+')
	console.log(q);
	fetch(`http://openlibrary.org/search.json?q=${q}`)

			.then (res=> res.json())
			.then(data =>{
				console.log(data);
				console.log(data.numFound);
					})
			setKeyword('');
}
// function hovered(){console.log("hovered")};
// function unhovered(){console.log("unhovered")}
// ANIMATION
// .nav-moon{
//   position: absolute;
//   height: 100px;
//   transform: rotate(0deg);
//   align-self: center !important;
//   z-index: 1000;
// }
// .logo-container:hover .nav-moon{
//   height: 55px;
//   margin-top:-12px;
//   margin-left:-15px;
//   transform: rotate(0deg);
//   opacity: 0;
//   transition: all 0.5s  ease-in-out;
// }

const hoverVariants={
	initial: {
	    opacity: 0,
	    scale: 0,
	    transform: "rotate(0deg)",
	    transition:{
	    	type: "tween",
	    	ease: "anticipate",
	    	duration: 0.5
	    }

	  },
	animate: {
	    opacity: 1,
	    scale: 1,
	    marginTop:"-12px",
	    marginLeft:"-15px",
	    transform: "rotate(5deg)",
	    transition:{
	    	type: "tween",
	    	ease: "anticipate",
	    	duration: 0.5
	    }

	  },
	nInitial: {
	    opacity: 0,
	    transform: "translateX(-75px)",
	    transition:{
	    	type: "tween", ease: "anticipate", duration: 0.5
	    }
	  },
	nAnimation: {
	    opacity: 1,
	    transform: "translateX(0px)",
	    transition:{
	    	type: "tween",
	    	ease: "anticipate",
	    	duration: 0.5
	    }

	  },
	out: {
	    opacity: 0,
	    y: "-100vh",
	    scale: 1.2,
	    transition:{
	    	type: "tween",
	    	ease: "anticipate",
	    	duration: 0.5,
	    	delay:0.4
	    }
	  }
}
	return(
		<Navbar expand={false} style={{zIndex: 1000, height:"100px"}} className="d-flex justify-content-center p-0">
				<div style={{height:"100px", width : "200px"}} className=" d-flex justify-content-center mt-0 pt-0 align-items-center">
					<span>
						<Navbar.Brand as={NavLink} to="/" className="d-flex justify-content-center logo-container"
								// onMouseEnter={()=> setIsHovered(true)}
								// onMouseLeave={()=> setIsHovered(false)}
						>	
							<img src={moon} className="moon" alt="moon"/>
							<img src={moonRotating} className="rotation" alt=""/>
							<div className="">
								<div>
									<div className="d-flex justify-content-center">
										<img src={mB} className="logo letter-m"  alt=""/>
										<img src={oB} className="logo letter-o" alt=""/>
										<img src={moonB} className="position-absolute moon-logo" alt=""/>
										<motion.img
/*										animate={isHovered? "nAnimation" : "nInitial"}
										variants={hoverVariants}*/
										 src={nB} className="logo letter-n" alt=""/>
									</div>
									<p className="reader-text">READER</p>	
								</div>
								

							</div>
							
						</Navbar.Brand>
					</span>
				</div>

		    
		</Navbar>
				

		)
		
	
}