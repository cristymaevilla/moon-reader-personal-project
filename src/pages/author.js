import { Button , Row , Col}from 'react-bootstrap';
import { NavLink, useLocation} from 'react-router-dom'; 
import {Fragment, useEffect, useState} from 'react';
import featureData from '../data/data'
import BookThumbnail from '../components/book-thumbnail';
import {motion} from 'framer-motion';
export default function Author(){
	const location = useLocation();
	const[author,setAuthor]=useState(featureData.author);
	const[authorKey,setAuthorKey]=useState(featureData.id);
	const [birthDate, setBirthDate] =useState("");
	const [bio, setBio] =useState("loading...");
	const[works, setWorks]= useState([]);

	// ANIMATION and PAGE TRANSITION:
	const transition = {duration : 0.6, ease:[0.6, 0.01, -0.05, 0.9]};
	const transitionVariants={
		initial: {
		    opacity: 0,
		    y: "100vh",
		    scale: 0.8
		  },
		animate: {
		    opacity: 1,
		    y: 0,
		    scale: 1
		  },
		out: {
		    opacity: 0,
		    y: "-100vh",
		    scale: 1.2
		  }
	}
	const pageTransition = {
	  type: "tween",
	  ease: "anticipate",
	  duration: 0.5,
	  delay:1
	};
	const thumbnailVariants={
		initial:{y:0},
		animate:{
			y:0,
			transition:{
				delayChildren:0.6
			}
		}
	}
// useEffect(()=>{
// 			// searchAuthor();
// 			authorBio();
// 			authorWorks()
// 			// authorBio2();
// 		},[])


// function searchAuthor(){
// 	const q = author.replace(" ", '%20')
// 	console.log(q);
// 	fetch(`https://openlibrary.org/search/authors.json?q=${q}`,
// 			// { mode: 'no-cors'

// 			// }
// 			)
// 			.then (res=> res.json())
// 			.then(data =>{
// 				setAuthorKey(data.docs[0].key)
// 					})
			
// };
useEffect(()=>{
	console.log(location); console.log(location.pathname);
	const authorBio=()=>{
		fetch(`https://openlibrary.org/authors/${authorKey}.json`)
				.then (res=> res.json())
				.then(data =>{
					// console.log(data);
					// let pattern = /\r/;
					let bio = data.bio.substring(0, data.bio.lastIndexOf("(") + 1);
					bio= bio.replace("(", "");
					setBirthDate(data.birth_date); setBio(bio);
						})
				
	}
	const authorWorks=()=>{
				fetch(`https://openlibrary.org/authors/${authorKey}/works.json`)
				.then (res=> res.json())
				.then(data =>{
					let entries = data.entries;
					let arr =[];
					for (let i = 0; i < entries.length; i++) {
						if(entries[i].description !== undefined){arr.push(data.entries[i]);}
						
					}
					
					let limitBooks = arr.slice(0, 18);
					setWorks(limitBooks.map(work =>{
						return( 
								<BookThumbnail key= {work.key}thumbnailProp ={work}/>
								)
					}))

					
				})
			}
		authorBio();
		authorWorks()
		},[])
function authorBio(){
	fetch(`https://openlibrary.org/authors/${authorKey}.json`)
			.then (res=> res.json())
			.then(data =>{
				// console.log(data);
				// let pattern = /\r/;
				let bio = data.bio.substring(0, data.bio.lastIndexOf("(") + 1);
				bio= bio.replace("(", "");
				setBirthDate(data.birth_date); setBio(bio);
					})
		
}
// var film = this.props.data.slice(0, 5).map((item) => {
//         return <FilmItem key={item.id} film={item} />
//     });
function authorWorks(){
			fetch(`https://openlibrary.org/authors/${authorKey}/works.json`)
			.then (res=> res.json())
			.then(data =>{
				let entries = data.entries;
				let arr =[];
				for (let i = 0; i < entries.length; i++) {
					if(entries[i].description !== undefined){arr.push(data.entries[i]);}
					
				}
				
				let limitBooks = arr.slice(0, 18);
				setWorks(limitBooks.map(work =>{
					return( 
							<BookThumbnail key= {work.key}thumbnailProp ={work}/>
							)
				}))

				
			})
		}

	

	return(
		<motion.div 
			initial="initial"
			animate="animate"
			exit="out"
			variants={transitionVariants}
			transition={pageTransition}
			className="d-flex-column justify-content-center text-center "

		>
			
			<h1 className="title">
				{featureData.author}
			</h1>
			<p>{birthDate}</p>
			<p>{bio}</p>
			<h2 >Some Works</h2>
			<motion.div
			variants={thumbnailVariants}
			>
			<Row 
			className="d-flex justify-content-center p-2 p-sm-5"> 
				<Fragment>
					{works}
				</Fragment>		
			</Row>
			</motion.div>
		</motion.div>
				

		)
}
		
	
