import { Button , Row , Col} from 'react-bootstrap';
// import Navbar from 'react-bootstrap/Navbar'
// import Nav from 'react-bootstrap/Nav';
import { Link} from 'react-router-dom';  
import { useEffect, useState} from 'react';

export default function Links(){
const[keyword,setKeyword]=useState('');


	return(
			<div className="justify-content-center d-flex flex-column text-center mt-5">
					<h1 className="card-link p-2">
						<Link to="/about" style={{textDecoration:"none"}}>What's this about?</Link>
					</h1>
					<h1 className="card-link p-2">
						<Link to="/author"  style={{textDecoration:"none"}}>Author of the Month</Link>
					</h1>
					<h1 className="card-link p-2">
						<Link to="/to-read-book" style={{textDecoration:"none"}}>To Read Book</Link>
					</h1>
					<h1 className="card-link p-2">
						<Link to="/search-book"  style={{textDecoration:"none"}}>Search for Book</Link>
					</h1>
				
			</div>


				

		)
		
	
}