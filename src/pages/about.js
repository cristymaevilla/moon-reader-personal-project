import { Button , Row , Col}from 'react-bootstrap';
import { NavLink, Link} from 'react-router-dom'; 
import featureData from '../data/data';
import { useEffect, useState} from 'react';
import BookInfo from '../components/book-card';
import {motion} from 'framer-motion';
// IMAGES
import aCenter from '../images/a-center.png'; 
import aSides from '../images/a-sides.png'; 
import fish from '../images/fish.png'; 
import bCenter from '../images/b-center.png'; 
import bLeft from '../images/b-left.png'; 
// import bRight from '../images/b-right.png'; 


export default function Works(){
	const [modalShow, setModalShow] =useState(false);
useEffect(()=>{
			console.log(featureData.bookId);
			book();
			console.log(featureData.imageurl)
},[])
function book(){
	fetch(`https://openlibrary.org/authors/${featureData.id}/works.json`)
		.then (res=> res.json())
		.then(data =>{
		console.log(data);
	
							})
}
	const transitionVariants={
		initial: {
		    opacity: 0,
		    y: "100vh",
		    scale: 0.8
		  },
		animate: {
		    opacity: 1,
		    y: 0,
		    scale: 1
		  },
		out: {
		    opacity: 0,
		    y: "-100vh",
		    scale: 1.2
		  }
	}
	const pageTransition = {
	  type: "tween",
	  ease: "anticipate",
	  duration: 0.5
	};


	return(
		<motion.div
		initial="initial"
		animate="animate"
		exit="out"
		variants={transitionVariants}
		transition={pageTransition} 		
		className="d-flex-column justify-content-center">

			<section className=" first-section-container ">
			<div className="moon-container">dd</div>
				<div className="d-flex justify-content-center">
					<span className="side-images-bg">
						<img src={aSides} className=" side-images " alt=""/>
					</span>
					<div className="d-flex justify-content-center p-0 m-0 top">
						<img src={aCenter} className="center-image" alt=""/>
						<h4 className="text-center qoute position-absolute top">For those who dream of stranger worlds</h4>
					</div>	
					<span className="side-images-bg">
						<img src={aSides} className=" horizontal-flip side-images" alt=""/>	
					</span>
					<img src={fish} className="fish-big" alt=""/>
					<img src={fish} className="fish-small horizontal-flip" alt=""/>	
				</div>
			</section>

			<div className="d-flex justify-content-center">
				<span className="">
					<img src={bLeft} className="side-images" alt=""/>	
				</span>	
				<div className="section2-moon section2-moon-responsiveness pt-4 pt-sm-5 text-center">
					I'm a <br/> 
					sucker for <br/>
					Young Adult <br/>
					books! 
				</div>
				<div className="d-flex justify-content-center p-0 m-0 top">
					<img src={bCenter} className="center-image" alt=""/>
					<h5 className="section2-description text-right position-absolute top col-6 col-sm-5 col-md-4 col-lg-4 col-xl-2 "> 
						This is a website where every month, I share with you one of my favorite authors and the book I wanted to read. <br/> <br/>
					 	The author and book information were fetched from openlibrary API. 
					 </h5>
				</div>	
				<span className="">
					<img src={bLeft} className="  side-images" alt=""/>	
				</span>	
			</div>

			<section className="text-center third-section-container "> 
			BEHIND THE SCENE 
			<div>
				images of the hand drawn illustrations (3 all in all)
				d-flex col-11 justify-content-center
				col-sm-8 col-md-5
			</div>
			
				<p>
					<span>THE CODE BASE  link</span>
					<span>  MY PORTFOLIO link</span>
				</p>

			</section>
	{/*		<Link style={{textDecoration: "none", color:"black"}} to="https://openlibrary.org/developers/api" exact>https://openlibrary.org/developers/api</Link>*/}
			
		

		</motion.div>	
				

		)
		
	
}