import { useEffect, useState} from 'react';
import { Button , Row , Col}from 'react-bootstrap';
import { NavLink, Link} from 'react-router-dom'; 
import featureData from '../data/data';
import card1 from '../images/card1.jpg'; 
import card2 from '../images/card3.jpg'; 
import card3 from '../images/card2.jpg'; 
import {motion} from 'framer-motion';
import Nav from 'react-bootstrap/Nav';

export default function Home(){

	const [wobble, setWobble] = useState(0);
	const [isPicked1, setIsPicked1] = useState(false);
	const [isPicked2, setIsPicked2] = useState(false);
	const [isPicked3, setIsPicked3] = useState(false);
	const transitionVariants={
		initial: {
		    opacity: 0,
		    y: "100vh",
		    scale: 0.8,
		    transition:{
		    	type: "tween",
		    	ease: "anticipate",
		    	duration: 0.5
		    }

		  },
		animate: {
		    opacity: 1,
		    y: 0,
		    scale: 1,
		    transition:{
		    	type: "tween",
		    	ease: "anticipate",
		    	duration: 0.5
		    }

		  },
		out: {
		    opacity: 0,
		    y: "-100vh",
		    scale: 1.2,
		    transition:{
		    	type: "tween",
		    	ease: "anticipate",
		    	duration: 0.5,
		    	delay:0.4
		    }
		  }
	}

	const cardExitVariants ={
		picked:{
			opacity: 1,
			y: "-5vh",
			scale: 1.1,
			transition:{
				type: "tween",
				ease: "anticipate",
				duration: 0.5,
			}
		},
		notPicked:{
			opacity: 0,
			y: "5vh",
			scale: 0.9,
			transition:{
				type: "tween",
				ease: "anticipate",
				duration: 0.4,
			}
		},
		cardLeftInt:{
			opacity: 0,
			x: "10vw",
			scale: 1,
			transition:{duration: 0.4,}
		},
		cardLeftAnimate:{
			opacity: 1,
			x: 0,
			scale: 1,
			transition:{duration: 0.4, delay: 0.3}
		},
		cardRightInt:{
			opacity: 0,
			x: "-10vw",
			scale: 1,
			transition:{duration: 0.4,}
		},
		cardRightAnimate:{
			opacity: 1,
			x: 0,
			scale: 1,
			transition:{duration: 0.4, delay: 0.3}
		}


	  };
	return(
		<motion.div 
		initial="initial"
		animate="animate"
		exit="out"
		variants={transitionVariants}
		className="d-flex flex-column justify-content-center text-center">
			<Row className="d-flex flex-sm-row flex-column justify-content-center align-items-middle m-0 mt-4 bg-stars order-2 order-sm-1 col-12">

				<motion.div 
				initial="cardLeftInt"
				animate="cardLeftAnimate"
				exit={isPicked1? "picked" : "notPicked"}
				variants={cardExitVariants}
				className="col-md-3 col-sm-4 d-flex align-items-middle justify-content-center card-wrapper p-3 pb-1"
				>
					<div>
						<img src={card1} className="rounded card cimg position-absolute shadow-1 fluid"/>
						<div className="rounded card cback shadow-1 ">
							<p className="card-number text-center pb-2">01</p>
							<h2 className="card-title-l">WHAT'S</h2>
							<h5 className="card-title-s">this all about?</h5>
							<h5 className="featured-text pt-md-3 pt-lg-4">Why on earth did I create this?</h5>
							<div className="bottom-text">
								<p className="card-subtitle pl-md-3 pr-md-3">Things to know about the website</p>
								<p className="card-link pb-2 m-0 mr-3 ml-3"
									exact onClick={() => setIsPicked1(true)}
									>
									<Link style={{textDecoration: "none", color:"white"}} to="/about" exact>PICK</Link>
								</p>
							</div>

						</div>	
					</div>
				</motion.div>

				<motion.div 
				exit={isPicked2? "picked" : "notPicked"}
				variants={cardExitVariants}
				className="col-md-3 col-sm-4 d-flex align-items-middle justify-content-center card-wrapper p-3 pb-1"
				>
					<div>
						<img src={card2} className="rounded card cimg position-absolute shadow-1 fluid"/>
						<div className="rounded card cback shadow-1 ">
							<p className="card-number text-center pb-2">02</p>
							<h2 className="card-title-l">AUTHOR</h2>
							<h5 className="card-title-s">of the month</h5>
							<h5 className="featured-text pt-md-3 pt-lg-4">
								{featureData.firstName}<br/> {featureData.lastName} <br/><br/>
							</h5>
							<div className="bottom-text">
							<p className="card-subtitle pl-sm-3 pr-sm-3 pl-2 pr-2">Know more about one of my favorite authors</p>
								<p className="card-link pb-2 m-0 mr-3 ml-3"
									exact onClick={() => setIsPicked2(true)}
									>
									<Link style={{textDecoration: "none", color:"white"}} to="/author" exact>PICK</Link>
								</p>
							</div>

						</div>	
					</div>
				</motion.div>

				<motion.div 
				initial="cardRightInt"
				animate="cardRightAnimate"
				exit={isPicked3? "picked" : "notPicked"}
				variants={cardExitVariants}
				className="col-md-3 col-sm-4 d-flex align-items-middle justify-content-center card-wrapper p-3 pb-1"
				>
					<div>
						<img src={card3} className="rounded card cimg position-absolute shadow-1 fluid"/>
						<div className="rounded card cback shadow-1 ">
							<p className="card-number text-center pb-2">03</p>
							<h2 className="card-title-l">THE BOOK</h2>
							<h5 className="card-title-s">to be read</h5>
							<h5 className="featured-text pt-md-3 pt-lg-4">
								{featureData.bookTitle1stL}<br/>
								{featureData.bookTitle2ndL}<br/>
								{featureData.bookTitle3rdL}<br/>
							</h5>
							<div className="bottom-text">
								<p className="card-subtitle pl-md-3 pr-md-3">Chosen TBR book from the author's works</p>
								<p className="card-link pb-2 m-0 mr-3 ml-3"
									exact onClick={() => setIsPicked3(true)}
									>
									<Link style={{textDecoration: "none", color:"white"}} to="/to-read-book" exact>PICK</Link>
								</p>
							</div>

						</div>	
					</div>
				</motion.div>

			</Row>
				
				<h2 className=" mt-sm-5 mb-sm-5 mt-3 order-1 order-sm-2 pick-or-die">pick or die</h2>
			
		</motion.div>	
				

		)
		
	
}
