import { Button , Row , Col, Container, Offcanvas, Form} from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav';
import { NavLink,  Link, useLocation, useParams} from 'react-router-dom'; 
import { useEffect, useState} from 'react';
// IMAGES:
import mB from '../images/m-b.svg'; 
import oB from '../images/o-b.svg';
import nB from '../images/n-b.svg';
import moonB from '../images/moon-b.svg';

import mW from '../images/m-w.svg';
import oW from '../images/o-w.svg';
import nW from '../images/n-w.svg';
import moonW from '../images/moon-w.svg';

import logo from '../images/logo.svg'; 
import moon from '../images/moon.svg'; 

export default function Author(){
// search for book title:
const[keyword,setKeyword]=useState('');
const[query,setQuery]=useState('');
const[dark,setDark]=useState(true);
const location = useLocation();
const {params} = useParams();
useEffect(()=>{
	console.log(location);
	 // showLocation()
		},[])
	
	
function show(){
	searchBook();
}
function showLocation(){
	console.log(location);console.log(params);
	console.log(`isdark? ${dark}`);
	if(location.pathname === "/"){setDark(true); console.log("dark")}
	else{setDark(false); console.log("light")}
}
function searchBook(){
	const q = keyword.replaceAll(" ", '+')
	console.log(q);
	fetch(`http://openlibrary.org/search.json?q=${q}`)

			.then (res=> res.json())
			.then(data =>{
				console.log(data);
				console.log(data.numFound);
					})
			setKeyword('');
}
function hovered(){console.log("hovered")};
function unhovered(){console.log("unhovered")}
	return(
		<Navbar expand={false} style={{zIndex: 1000, height:"100px"}} className="d-flex justify-content-center p-0">
			{dark?
				<div style={{height:"100px", width : "200px"}} className=" d-flex justify-content-center mt-0 pt-0 align-items-center">
					<span>
						<Navbar.Brand as={NavLink} to="/" className="d-flex justify-content-center logo-container"
								onMouseEnter={hovered}
								onMouseLeave={unhovered}
						>	
							<img src={moon} className="nav-moon" alt="moon"/>
							<div className="nav-fulltext">
								<div>
									<div className="d-flex justify-content-center position-absolute">
										<img src={mB} className="" alt=""/>
										<div>
											<img src={oB} className="" alt=""/>
											<img src={moonB} className="position-absolute" alt=""/>
										</div>
										<img src={nB} className="" alt=""/>
									</div>
									<p className="nav-text">READER</p>	
								</div>
								

							</div>
							
						</Navbar.Brand>
					</span>
				</div>
			:
				
			
			<div style={{height:"100px", width : "200px"}} className="d-flex justify-content-center mt-0 pt-0 align-items-center">
				<span>
					<Navbar.Brand as={NavLink} to="/">
						<div>
							<img src={logo} className="nav-logo" alt="logo"/>
						</div>
						<p className="nav-text">R  E  A  D  E  R</p>
					</Navbar.Brand>
				</span>
			</div>
		}
		    
		</Navbar>
				

		)
		
	
}