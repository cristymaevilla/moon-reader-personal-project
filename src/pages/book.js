import { Button , Row , Col}from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import { NavLink} from 'react-router-dom'; 
import { useEffect, useState} from 'react';
import bookimage2 from '../images/bookimage2.png';
import featureData from '../data/data';
import Image from 'react-bootstrap/Image';
import {motion} from 'framer-motion';

export default function Books(){
	const [description, setDescription] =useState("loading...     ");
	const [title, setTitle] =useState("");
	const [showAllText, setShowAllText]= useState(false);
	const [isHidden, setIsHidden]= useState(false);


useEffect(()=>{
			book();
			console.log(showAllText)
		},[])

function book(){
	console.log(featureData.bookId);
	fetch(`https://openlibrary.org/works/${featureData.bookId}.json`)
			.then (res=> res.json())
			.then(data =>{
				 // let newdescription =data.description.replace("*", '');
				 // console.log(newdescription)
				setDescription(data.description);
				setTitle(data.title);
					})
}

	const transitionVariants={
		initial: {
		    opacity: 0,
		    y: "100vh",
		    scale: 0.8
		  },
		animate: {
		    opacity: 1,
		    y: 0,
		    scale: 1
		  },
		out: {
		    opacity: 0,
		    y: "-100vh",
		    scale: 1.2
		  }
	}
	const pageTransition = {
	  type: "tween",
	  ease: "anticipate",
	  duration: 0.5
	};
	const showHideVariants={
		animate:{
			display:"block",
			transition:{duration: 0.4}
		},
		exit:{
			display:"none",
			transition:{duration: 0.4}
		}
	}
	const descriptionVariants={
		initial:{
			opacity:0,
			y:5,
			duration:0.5	
		},
		animate:{
			opacity:1,
			y:0,
			transition:{duration: 0.3, delay: 0.1}
		},
		exit:{
			opacity:0,
			y:-5,
			transition:{duration: 0.4}
		}
	}
	return(
	<motion.div
	initial="initial"
	animate="animate"
	exit="out"
	variants={transitionVariants}
	transition={pageTransition}
	 className=" pt-2 d-flex-column justify-content-center to-read-book" >
		<div className="p-1  p-lg-1 ">
			<div className="d-md-flex align-items-top">
				<div className="container-fluid p-0 bg-none p-3 p-md-0 bg-primary col-md-5">
	
						<Image  src={bookimage2} fluid/>

				</div>

				<div className="col-md-7 p-3 text-center">
					<h1 className="title">
						{featureData.bookTitle}
					</h1>
					<div className="">
						<p className=" published">
							<em>{title}</em>
						</p>
						<div style={{overflowY:"hidden"}}>

							<motion.div
							initial="exit"
							animate={showAllText? "animate" : "exit"}
							exit={showAllText? "animate" : "exit"}
							variants={showHideVariants}>
								<motion.p
								initial="exit"
								animate={showAllText? "animate" : "exit"}
								exit={showAllText? "animate" : "exit"}
								variants={descriptionVariants}
								 className="pl-2 pl-sm-2 pl-md-5 pr-2 pr-sm-2 pr-md-5 ">
									{description}
								</motion.p>
								<p className="show-text-button" onClick={()=>{setShowAllText(false); setIsHidden(false)}}>
									HIDE
								</p>
							</motion.div>
						
							<motion.div
							initial="exit"
							animate={isHidden? "exit" : "animate"}
							exit={showAllText? "animate" : "exit"}
							variants={showHideVariants}>
								<p className="pl-2 pl-sm-2 pl-lg-5 pr-2 pr-sm-2 pr-lg-5 line-clamp">
									{description}
								</p>
								<p className="show-text-button" onClick={()=>{setShowAllText(true); setIsHidden(true)}}>
									SHOW ALL
								</p>
							</motion.div>
						
						</div>
					</div>
					<Nav.Link className=" text-center bg-primary col-10 col-sm-10 col-lg-10 col-xl-8 m-auto mt-5" as={NavLink} to="/author" exact>
						Some works written by {featureData.author}
					</Nav.Link>
					<Nav.Link as={NavLink} to="/author" exact>Works of Author</Nav.Link>
				</div>
			</div>

		</div>

	</motion.div>
		)
		
	
}