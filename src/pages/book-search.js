import { Button , Row , Col, Container, Offcanvas, Form} from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav';
import { NavLink,  Link} from 'react-router-dom'; 
import { Fragment, useEffect, useState} from 'react';
import ResultThumbnail from '../components/search-thumbnail';
// images:

import searchIcon from '../images/search-icon.svg'; 
import searchLoader from '../images/search-loader.gif'; 

export default function Author(){
// search for book title:
const[keyword,setKeyword]=useState('');
const[query,setQuery]=useState('');
const[results,setResults]=useState('');
const[notFound,setNotFound]=useState(false);
const[isSearching,setisSearching]=useState(false);
function show(){
	searchBook();

}
function searchBook(){
	const q = keyword.replaceAll(" ", '+')
	console.log(q);
	fetch(`http://openlibrary.org/search.json?q=${q}`)
	
			.then (res=> res.json())
			.then(data =>{
				console.log(data);
				console.log(data.numFound);
					})
			setKeyword('');
}

function searchBook2(){
	setisSearching(true);
	const q = keyword.replaceAll(" ", '+'); console.log(q);
		fetch(`http://openlibrary.org/search.json?q=${q}`)
		.then (res=> res.json())
		.then(data =>{
		let searchresult = data.docs;	
			let arr =[];
		if(data.numFound !== 0){
			for (let i = 0; i < searchresult.length; i++) {
				if( searchresult[i].cover_i !== undefined){
									arr.push(searchresult[i]);
							}
			}
			let limitBooks = arr.slice(0, 30);
			setisSearching(false);
			setResults(limitBooks.map(result =>{
				
				return( 
						<ResultThumbnail key= {result.key}thumbnailProp ={result}/>
						)
			})) 

		}else{
			setisSearching(false);
			setNotFound(true);

		}			
	})
}
	return(
			<div className="d-flex flex-column justify-content-center text-center">
				<div className="d-flex justify-content-center">
{/*					<Form className="d-flex justify-content-center align-items-center ml-5 p-3 col-lg-5  col-sm-8 col-12">
					  <Form.Control
					    type="search"
					    placeholder="Enter Book Title"
					    value={keyword}
					    aria-label="Search"
					    onChange={(e) => setKeyword(e.target.value)}
					  />
					  <img style={{height:"30px", width :"30px"}} src={searchIcon} className="p-1 " alt=""/>
					</Form>*/}
					<form className="d-flex justify-content-center align-items-center ml-5 p-0 col-lg-5 col-sm-7 col-12 search-bar">
					  <input 
					  	type="text" 
					  	id="fname" 
					  	name="fname" 
					  	placeholder="Enter Book Title"
					  	value={keyword}
						onChange={(e) => setKeyword(e.target.value)}
						className="text-center col-11"
					  	/>
					  <img style={{height:"30px", width :"30px"}} src={searchIcon} className="p-1 pl-2 " alt=""/>
					</form>
				</div>
				<div  className="d-flex justify-content-center">
					<Button variant="light col-lg-2  col-sm-3 col-4" onClick ={searchBook2}>Search</Button>
				</div>
				
				<h4 className="mt-3">Search Books from Open Library</h4>
				{isSearching?
					<div>Please wait. This make take some time 
					<img src={searchLoader} className=" " alt="loading gif"/>
					</div>
					:
					<div></div>
				}
				<Row 
				className="d-flex justify-content-center p-2 p-sm-5"> 
					<Fragment>
						{results}
					</Fragment>		
				</Row>
			</div>


				

		)
		
	
}